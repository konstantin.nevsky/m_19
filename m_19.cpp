// m_19.cpp
//

#include <iostream>

class Animal
{
public:
    Animal() {}
    virtual void Voice() 
    {
        std::cout << "I am an animal!" << std::endl;
    }
};

class Cat : public Animal
{
public:
    Cat() {}
    void Voice() override
    {
        std::cout << "Meow!" << std::endl;
    }
};

class Dog : public Animal
{
public:
    Dog() {}
    void Voice() override
    {
        std::cout << "Woof!" << std::endl;
    }
};

class Duck : public Animal
{
public:
    Duck() {}
    void Voice() override
    {
        std::cout << "Quack!" << std::endl;
    }
};



int main()
{
    Animal* Pet_1 = new Cat();
    
    Animal* Pet_2 = new Dog();
 
    Animal* Pet_3 = new Duck();

    Animal* Pets[] = {Pet_1, Pet_2, Pet_3};

    for (Animal* animal : Pets)
    {
        animal->Voice();
    }

    delete Pet_1;
    Pet_1 = nullptr;
    delete Pet_2;
    Pet_2 = nullptr;
    delete Pet_3;
    Pet_3 = nullptr;
}

